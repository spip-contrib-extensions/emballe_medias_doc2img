<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/em_doc2img?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_nb_doc2img' => 'Este documento ha sido automáticamente convertido en una serie de @nb@ imágenes.',
	'info_nb_doc2img_un' => 'Este documento ha sido automáticamente convertido en una imagen.'
);
