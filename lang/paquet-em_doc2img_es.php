<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-em_doc2img?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'em_doc2img_description' => 'Complemento Doc2Img para "Envuelve Medias" (utiliza doc2img)',
	'em_doc2img_nom' => 'Envuelve Medias - Doc2Img',
	'em_doc2img_slogan' => 'Complemento Doc2Img para "Envuelve Medias" (utiliza doc2img)'
);
