<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/emballe_medias_doc2img.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'em_doc2img_description' => 'Complément Doc2Img pour "Emballe Medias" (utilise doc2img)',
	'em_doc2img_nom' => 'Emballe Medias - Doc2Img',
	'em_doc2img_slogan' => 'Complément Doc2Img pour "Emballe Medias" (utilise doc2img)'
);
